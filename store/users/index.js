import _ from 'lodash'

export default {
  state: {
    users: null,
    usersFilter: null
  },
  mutations: {
    setUsers(state, payload) {
      state.users = payload
    },
    setUsersFilter(state, payload) {
      state.usersFilter = payload
    }
  },
  actions: {
    loadUsers({ commit }, payload) {
      commit('setLoading', true)
      this.$axios
        .get('/db.json')
        .then(res => {
          commit('setUsers', res.data)
          commit('setLoading', false)
        })
        .catch(error => {
          commit('setError', {
            message: error
          })
          commit('setLoading', false)
        })
    }
  },
  getters: {
    users(state) {
      if (state.usersFilter) {
        if (state.usersFilter === 'male') {
          const users = _.filter(state.users, function(user) {
            return user.gender === 'male'
          })
          return users
        } else if (state.usersFilter === 'female') {
          const users = _.filter(state.users, function(user) {
            return user.gender === 'female'
          })
          return users
        } else if (state.usersFilter === 'overThirty') {
          const users = _.filter(state.users, function(user) {
            return user.age >= 30
          })
          return users
        } else if (state.usersFilter === 'underThirty') {
          const users = _.filter(state.users, function(user) {
            return user.age <= 30
          })
          return users
        }
      } else {
        return state.users
      }
    },
    usersFilter(state) {
      return state.usersFilter
    }
  }
}
