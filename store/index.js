import Vue from 'vue'
import Vuex from 'vuex'

import shared from './shared'
import users from './users'

Vue.use(Vuex)

const createStore = () => {
  return new Vuex.Store({
    modules: {
      shared: shared,
      users: users
    }
  })
}

export default createStore
