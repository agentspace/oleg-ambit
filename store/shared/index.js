export default {
  state: {
    loading: false,
    success: null,
    error: null
  },
  mutations: {
    setLoading(state, payload) {
      state.loading = payload
    },
    setSuccess(state, payload) {
      state.success = payload
    },
    setError(state, payload) {
      state.error = payload
    }
  },
  getters: {
    loading(state) {
      return state.loading
    },
    success(state) {
      return state.success
    },
    error(state) {
      return state.error
    }
  }
}
